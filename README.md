# README #

This is a game that uses augmented reality to simulate the presence of a global epidemic. Work with others to bring the infection rate of your region to 0%!

Play here>>https://augmented-pandemic.herokuapp.com/game/

## How to play

Choose an action (quarantine, cure, or rescue) to try and bring down the infection rate of your region! 

* Product Owner: Ryan Walters
* SCRUM: Sharon Huang
* Developer: Ted Shin

## How to install as developer

* pip install -r requirements.txt
* python manage.py loaddata initial.json
* python manage.py runserver and go to http://127.0.0.1:8000/game/ to see the homepage

## How to run mochaTestrunner.html
* npm install
* Open mochaTestrunner.html in a web browser to run javascript tests