from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(10)

    def tearDown(self):
        self.browser.quit()

    def test_site_startup(self):
        self.browser.get('http://localhost:8000/game')
        self.assertIn('Augmented Pandemic Home Page', self.browser.title) # user sees homepage title
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Announcements', header_text) # user can see the announcements on the homepage

    def test_login(self): # player should login to be able to save their progress
        self.browser.get('http://localhost:8000/login')
        self.assertIn('Login', self.browser.title)

        username = self.browser.find_element_by_name('username')
        username.send_keys('fighter')

        password = self.browser.find_element_by_name('password')
        password.send_keys('pandemic')

        self.browser.find_element_by_xpath('//input[@type="submit"]').click()

        self.assertEqual(self.browser.current_url, 'http://localhost:8000/game/play/')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
